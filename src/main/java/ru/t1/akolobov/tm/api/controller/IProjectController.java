package ru.t1.akolobov.tm.api.controller;

import ru.t1.akolobov.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void displayProjects();

    void displayProject(Project project);

    void displayProjectById();

    void displayProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

}
