package ru.t1.akolobov.tm.api.controller;

import ru.t1.akolobov.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void displayTasks();

    void displayTask(Task task);

    void displayTaskById();

    void displayTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

}
