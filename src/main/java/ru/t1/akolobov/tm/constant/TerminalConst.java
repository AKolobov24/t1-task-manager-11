package ru.t1.akolobov.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_INFO = "info";
    public static final String CMD_EXIT = "exit";
    public static final String CMD_ARGUMENTS = "arguments";
    public static final String CMD_COMMANDS = "commands";
    public static final String CMD_PROJECT_CREATE = "project-create";
    public static final String CMD_PROJECT_CLEAR = "project-clear";
    public static final String CMD_PROJECT_LIST = "project-list";
    public static final String CMD_PROJECT_DISPLAY_BY_ID = "project-display-by-id";
    public static final String CMD_PROJECT_DISPLAY_BY_INDEX = "project-display-by-index";
    public static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";
    public static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String CMD_TASK_CREATE = "task-create";
    public static final String CMD_TASK_CLEAR = "task-clear";
    public static final String CMD_TASK_LIST = "task-list";
    public static final String CMD_TASK_DISPLAY_BY_ID = "task-display-by-id";
    public static final String CMD_TASK_DISPLAY_BY_INDEX = "task-display-by-index";
    public static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";

}
